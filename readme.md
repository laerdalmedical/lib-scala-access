Access Library
==============
This scala library contains various convenience functions for accessing tables and blobs in the analytics platform.

How to use
----------
This section assumes that the library has been installed on the cluster. To install, see section "Installation" below.

The library currently contains two objects:

 - `Tables` for loading the tables in the analytics platform.
 - `BlobMetrics` for loading blobs and extracting scores.
  
To use `Tables` you can e.g. do:
```
import com.laerdal.access.Tables

val csProgress = Tables.Load.csProgress()
```
This loads the csProgress bronze table. The `Tables` object loads the `raw_import_config.json` configuration file so it should resolve to the table relevant for the environment and region.

The `Tables.Load` object has functions for most bronze and gold tables. However the tables are added by need so if one is missing, let me know. 

The `BlobMetrics` object loads blobs and extracts sub-scores from them. Currently it only supports SSX-files and it fetches them directly from the content service api.

NOTE: As the `BlobMetrics` object loads directly from the content service api, **it should definitely *not* be used to bulk load blobs!**

To use the `BlobMetrics` object, use e.g.:
```
import com.laerdal.access.BlobMetrics

var password = /* REDACTED! */
var blobs = BlobMetrics.euCs(password)

var metrics = blob.getSsxMetrics("5C23E33A-FCFB-E911-822E-02634E793282")
```
This will load all the sub-scores from the blob with blob id `"5C23E33A-FCFB-E911-822E-02634E793282"` in the EU content service (`cs2ne.contentservice.net`) and return a dataframe with columns:
-  `id` - the blob id.
-  `key` - the id of the sub-score.
-  `value` - the value of the sub-score in the range 0–1.

The `password` is the password of the `datascience` user in the given region. To get the password, ask Allan or somebody.

Installation
------------
To install the library yourself, you need to build the library and upload it to the cluster.

To build the library you need to have a JDK and SBT installed. See how on:

 - https://www.scala-sbt.org/1.x/docs/Setup.html

Once JDK and SBT are installed use the terminal command:
```
sbt package
```
in the root folder of the `lib-scala-access` repository. This will generate a jar file in the `lib-scala-access/taget/scala-2.11` folder if everything goes right. This jar file can be uploaded to your cluster. If the version numbers do not match, also remember to remove any previous instance of the library.

Reference
---------
For further documentation, see the scaladocs in:

 - [scaladocs](target/scala-2.11/api/index.html)