name := "lib-access"
version := "0.0.3"
scalaVersion := "2.11.12"
organization := "com.laerdal"

libraryDependencies += "com.amazonaws" % "aws-java-sdk" % "1.11.550"
libraryDependencies += "com.amazonaws" % "aws-java-sdk-core" % "1.11.550"
libraryDependencies += "com.typesafe" % "config" % "1.3.4"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.5"
libraryDependencies += "org.scalaj" %% "scalaj-http" % "2.4.1"
libraryDependencies += "com.databricks" % "spark-xml_2.11" % "0.9.0"
libraryDependencies += "io.spray" % "spray-json_2.11" % "1.3.5"
