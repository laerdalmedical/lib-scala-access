package com.laerdal
package access

/**
 *  Load metrics from blobs assuming it is an SSX file.
 * 
 * @param cs_ The content service (e.g. "cs2ne.contentservice.net")
 * @param auth_ The authentication service (e.g. "eco-ne.contentservice.net")
 * @param password_ The password of the "datascience" user on the authentication service.
 * @todo Currently loads through CS api. Load from azure api or bronze.
 */
class BlobMetrics(cs_ : String, auth_ : String, password_ : String) {
    import org.apache.spark.sql.DataFrame
    import org.apache.spark.sql.Row
  
    /**
     *  Gets metrics from the SSX file with the given id. 
     * 
     * @param id The id of the SSX file.
     * @return A dataframe with columns (id, key, value)
     */
    def getSsxMetrics(id : String) : DataFrame = {
        (for {
            blob <- Inner.getBlob(id)
            cprEvents <- Inner.getCprEvents(blob)
        } yield Inner.getMetrics(Inner.toDf(id, cprEvents)))
            .getOrElse(Inner.spark.createDataFrame(Inner.spark.sparkContext.emptyRDD[Row], Inner.schema))
    }
  
    /** 
     * Gets the blob file with the given id.
     * 
     * @param id The id of the blob file.
     * @return The blob as a string or the empty string if no such blob exists.
     */
    def getBlobAsString(id : String) : String = {
        Inner.getBlobAsString(id)
          .getOrElse("")
    }
  
    object Config {
        val cs = cs_
        val auth = auth_
        val authUser = "datascience"
        val authPassword = password_
    }
  
    object Inner {
        import org.apache.spark.sql.SparkSession
        
        val spark = SparkSession.builder().getOrCreate()
        import spark.implicits._

        /** Gets a blob by id returning the response body as bytes on success. */
        def getBlob(id : String) : Option[Array[Byte]] = get(formatBlobUrl(id))

        def getBlobAsString(id : String) : Option[String] = getAsString(formatBlobUrl(id))

        /** Formats a blob id as a blob request url. */
        def formatBlobUrl(id : String) : String = s"https://${Config.cs}/api/internal/v1/blob/$id"

        /** 
         * Gets an url and returns the response body as bytes on success. 
         * 
         * @todo Retry
         */
        def get(url : String) : Option[Array[Byte]] = {
            import scalaj.http.Http
            val response = Http(url)
                .header("Authorization", s"Bearer ${getToken()}")
                .asBytes
            if (response.code == 200) Some(response.body)
            else {
                println(s"Failed to GET $url: ${response.code}")
                None
            }
        }
    
        def getAsString(url : String) : Option[String] = {
            import scalaj.http.Http
            val response = Http(url)
                .header("Authorization", s"Bearer ${getToken()}")
                .asString
            if (response.code == 200) Some(response.body)
            else {
                println(s"Failed to GET $url: ${response.code}")
                None
            }      
        }

        def getCprEvents(bytes : Array[Byte]) : Option[String] = getZipFile(bytes, "CPR/CPREvents.xml")
    
        /** Gets a file inside a zip file as text. */
        def getZipFile(bytes : Array[Byte], path : String) : Option[String] = {
            import java.io.ByteArrayInputStream
            import org.apache.commons.codec.binary.Base64InputStream
            import java.util.zip.ZipInputStream
            import org.apache.commons.io.IOUtils
            import java.nio.charset.Charset
            
            val zipStream = new ZipInputStream(new Base64InputStream(new ByteArrayInputStream(bytes)))
            var zipEntry = zipStream.getNextEntry()
            var text : Option[String] = None
            while (zipEntry != null) {
                if (zipEntry.getName() == path) {
                    text = Some(IOUtils.toString(zipStream, Charset.forName("ISO-8859-1")))
                }
                zipEntry = zipStream.getNextEntry()
            }
            //TODO: close stream?
            text
        }
    
        /** 
         * Parses `s` as an CPREvents xml and creates a dataframe.
         * 
         * @param id Id of the metadata.
         * @param xml The xml string.
         * @return A dataframe with columns (id, xml).
         */
        def toDf(id : String, s : String) : DataFrame = {
            import com.databricks.spark.xml.functions.from_xml
            
            spark.createDataFrame(Seq(Tuple2(id, s)))
                .select(
                    $"_1" as "id",
                    $"_2" as "string"
                )
                .withColumn("xml", from_xml($"string", schema))
                .select("id", "xml")
        }

        import org.apache.spark.sql.types._
        val schema = StructType(
            StructField("evt", 
                ArrayType(
                    StructType(
                        StructField("_msecs", LongType, true) ::
                        StructField("_type", StringType, true) ::
                        StructField("param",
                            ArrayType(
                                StructType(
                                    StructField("_type", StringType, true) ::
                                    StructField("_units", StringType, true) :: 
                                    StructField("_value", StringType, true) ::
                                    Nil
                                ),true
                            ),true
                        ) :: 
                        Nil
                    ),true
                ),true
            ) ::
            Nil
        )
    
        /** 
         * Extracts metrics from a CPREvents xml dataframe.
         * 
         * @param df A dataframe with columns (id, xml).
         * @return A dataframe with columns (id, key, value)
         * @todo Verify all units are percent.
         */
        def getMetrics(df : DataFrame) : DataFrame = {
            import org.apache.spark.sql.sources.IsNull
            import org.apache.spark.sql.functions.explode
            
            df
                .select($"id", explode($"xml.evt") as "evt")
                .filter($"evt._type" === "CprSessionScore")
                .select($"id", explode($"evt.param") as "params")
                .select(
                    $"id",
                    $"params._type" as "key",
                    $"params._value" as "rawValue",
                    $"params._units" as "units",
                    $"params._value".cast(DoubleType) as "floatValue"
                )
                .filter(!$"units".isNull && $"floatValue" >= 0)
                .select(
                    $"id",
                    $"key",
                    $"floatValue" / 100 as "value"
                )
        }

        /** Gets an access token from `Config.auth` */
        def getToken() : String = {
            import scalaj.http.Http
            import spray.json._
            import DefaultJsonProtocol._
            
            try {
                case class AToken(accessToken: String)
                implicit val ATokenFormat = jsonFormat1(AToken)
                val source = """{"expiry": 600, "scope": {"org": "256" }}"""
                val accessToken: String = Http(s"https://${Config.auth}/auth/v1/token")
                    .postData(source)
                    .header("content-type", "application/json")
                    .auth(Config.authUser, Config.authPassword)
                    .asString.body.parseJson.convertTo[AToken].accessToken
                accessToken;
            } catch {
                case e: java.io.IOException => {
                    println("error occured " + e);
                    "";
                }
            }
        }

    }
}

object BlobMetrics {
    /**
      * Gets metrics through the content service api for EU 
      * (cs2ne.contentservice.net).
      *
      * @param password The password for the datascience user.
      * @return A BlobMetrics for retrieving EU blobs.
      */
    def euCs(password : String) : BlobMetrics =
        new BlobMetrics(Cs.eu, Auth.eu, password)

    object Cs {
        def eu = "cs2ne.contentservice.net"
    }
    object Auth {
        def eu = "eco-ne.contentservice.net"
    }
}