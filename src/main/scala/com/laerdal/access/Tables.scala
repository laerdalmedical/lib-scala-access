package com.laerdal
package access

/**
 * An object for accessing various Analytics tables.
 * 
 * Built lazily.
 */
object Tables {
    import org.apache.spark.sql.DataFrame

    /** Loads various tables as dataframes. */
    object Load {
        /** Loads the cs_attempt table from bronze. */
        def csAttempt() : DataFrame = Tools.loadDelta(Path.csAttempt)
        
        /** Loads the cs_idmap table from bronze. */
        def csIdmap() : DataFrame = Tools.loadDelta(Path.csIdmap)
        
        /** Loads the cs_moc table from bronze. */
        def csMoc() : DataFrame = Tools.loadDelta(Path.csMoc)
        
        /** Loads the cs_moc_progress table from bronze. */
        def csMocProgress() : DataFrame = Tools.loadDelta(Path.csMocProgress)
        
        /** Loads the cs_progress table from bronze. */
        def csProgress() : DataFrame = Tools.loadDelta(Path.csProgress)

        /** Loads the cs_upload_meta_data table from bronze. */
        def csUploadMetaData() : DataFrame = Tools.loadDelta(Path.csUploadMetaData)  

        /** Loads the cs_user table from bronze. */
        def csUser() : DataFrame = Tools.loadDelta(Path.csUser)

        /** Loads the cs_userblobs table from bronze. */
        def csUserblobs() : DataFrame = Tools.loadDelta(Path.csUserblobs)

        /** Loads the org_unit table from bronze. */
        def orgunit() : DataFrame = Tools.loadDelta(Path.orgunit)
        
        /** Loads the organization table from bronze. */
        def organization() : DataFrame = Tools.loadDelta(Path.organization)

        def activityMetricsGold() : DataFrame = Tools.loadDelta(Path.activityMetricsGold)
        def agentGold() : DataFrame = Tools.loadDelta(Path.agentGold)
        def complianceGold() : DataFrame = Tools.loadDelta(Path.complianceGold)
        def orgUnitGold() : DataFrame = Tools.loadDelta(Path.orgUnitGold)
    }
  
    /** Paths to various tables. */
    object Path {
        /** The path to the cs_attempt table. */
        val csAttempt : String = Tools.combine(Area.bronze, Table.csAttempt)
        val csIdmap : String = Tools.combine(Area.bronze, Table.csIdmap)
        val csMoc : String = Tools.combine(Area.bronze, Table.csMoc)
        val csMocProgress : String = Tools.combine(Area.bronze, Table.csMocProgress)

        /** The path to the progress table. */
        val csProgress : String = Tools.combine(Area.bronze, Table.csProgress)

        /** The path to the metadata table. */
        val csUploadMetaData : String = Tools.combine(Area.bronze, Table.csUploadMetaData)

        val csUser : String = Tools.combine(Area.bronze, Table.csUser)
        val csUserblobs : String = Tools.combine(Area.bronze, Table.csUserblobs)
        val orgunit : String = Tools.combine(Area.bronze, Table.orgunit)
        val organization : String = Tools.combine(Area.bronze, Table.organization)

        val activityMetricsGold : String = Tools.combine(Area.gold, Table.activityMetricsGold)
        val agentGold : String = Tools.combine(Area.gold, Table.agentGold)
        val complianceGold : String = Tools.combine(Area.gold, Table.complianceGold)
        val orgUnitGold : String = Tools.combine(Area.gold, Table.orgUnitGold)

        /** The path to the rawFiles table. */
        val rawFiles : String = Tools.combine(Area.bronze, Table.rawFiles)

        /** The path to a specific blob in rawFiles. */
        def rawFile(source : String, year : String, month : String, day : String, id : String) : String =
            s"$rawFiles/source=$source/year=$year/month=$month/day=$day/$id"
    }

    object Area {
        val bronze = Tools.config.getBucketPath("rawBucket") + "/0001/cs"
        val gold = Tools.config.getBucketPath("refinedLowSensitivityBucket") + "/0003/"
    }
  
    object Table {
        val csAttempt = "cs_attempt"
        val csIdmap = "cs_idmap"
        val csMoc = "cs_moc"
        val csMocProgress = "cs_moc_progress"
        val csProgress = "cs_progress"
        val csUploadMetaData = "cs_upload_meta_data"
        val csUser = "cs_user"
        val csUserblobs = "cs_userblobs"
        val orgunit = "org_unit"
        val organization = "organization"
        val rawFiles = "rawFiles"
        
        val activityMetricsGold = "ActivityMetricsGold"
        val agentGold = "AgentGold"
        val complianceGold = "ComplianceGold"
        val orgUnitGold = "OrgUnitGold"
    }
  
    object Tools {
        import org.apache.spark.sql.SparkSession
        import com.laerdal.config.Config

        val spark = SparkSession.builder().getOrCreate()
        val config = new Config(s"/dbfs/raw_import_config.json", true)

        /** 
         * Loads a delta file at `path` as a dataframe. 
         * 
         * @param path Path of the delta file.
         */
        def loadDelta(path : String) : DataFrame = 
            spark.read.format("delta").load(path).cache()

        /**
         *  Combine an area name and a table name into a path.
         * 
         * @param area The area of the path (e.g. Area.bronze)
         * @param table The table (e.g. Table.csProgress)
         */
        def combine(area : String, table : String) : String = s"$area/$table"
    }

}
